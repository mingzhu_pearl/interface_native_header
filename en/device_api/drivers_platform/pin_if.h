/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 *
 * HDF is dual licensed: you can use it either under the terms of
 * the GPL, or the BSD license, at your option.
 * See the LICENSE file in the root of this repository for complete details.
 */

/**
 * @file pin_if.h
 *
 * @brief Declares the standard pin APIs.
 *
 * @since 3.1
 */

#ifndef PIN_IF_H
#define PIN_IF_H

#include "platform_if.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief Enumerates the pin's pull types.
 *
 * @since 3.1
 */
enum PinPullType {
    /** Floating */
    PIN_PULL_NONE = 0,
    /** Pull-up */
    PIN_PULL_UP =  1,
    /** Pull-down */
    PIN_PULL_DOWN = 2,
};

/**
 * @brief Obtains the device handle of a pin.
 * This API must be called before the pin attributes are set.
 *
 * @param pinName Indicates the pointer to the name of the target pin.
 *
 * @return Returns the pin device handle if the operation is successful.
 * @return Returns <b>NULL</b> if the operation fails.
 *
 * @since 3.1
 */
DevHandle PinGet(const char *pinName);

/**
 * @brief Releases the device handle of this pin.
 * This API is used to release unused memory resources when a pin does not need to be set.
 *
 * @param handle Indicates the device handle to release.
 *
 * @since 3.1
 */
void PinPut(DevHandle handle);

/**
 * @brief Sets the pull type of a pin.
 * 
 *
 * @param handle Indicates the device handle of the pin to set.
 * @param pullType Indicates the pull type to set.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
int32_t PinSetPull(DevHandle handle, enum PinPullType pullType);

/**
 * @brief Obtains the pull type of a pin.
 * 
 *
 * @param handle Indicates the device handle of the target pin.
 * @param pullType Indicates the pointer to the pull type obtained.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
int32_t PinGetPull(DevHandle handle, enum PinPullType *pullType);

/**
 * @brief Sets the pull strength of a pin.
 * 
 *
 * @param handle Indicates the device handle of the target pin.
 * @param strength Indicates the pull strength to set.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
int32_t PinSetStrength(DevHandle handle, uint32_t strength);

/**
 * @brief Obtains the pull strength of a pin.
 * 
 *
 * @param handle Indicates the device handle of the target pin.
 * @param strength Indicates the pointer to the pull strength obtained.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
int32_t PinGetStrength(DevHandle handle, uint32_t *strength);

/**
 * @brief Sets the pin function.
 * 
 *
 * @param handle Indicates the device handle of the target pin.
 * @param funcName Indicates the pointer to the function to set.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
int32_t PinSetFunc(DevHandle handle, const char *funcName);

/**
 * @brief Obtains the pin function.
 *
 *
 * @param handle Indicates the device handle of the target pin.
 * @param funcName Indicates the double pointer to the pin function obtained.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
int32_t PinGetFunc(DevHandle handle, const char **funcName);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* PIN_IF_H */
