/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_NATIVE_BUFFER_H_
#define NDK_INCLUDE_NATIVE_BUFFER_H_

/**
 * @addtogroup OH_NativeBuffer
 * @{
 *
 * @brief 提供NativeBuffer功能
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @since 9
 * @version 1.0
 */

/**
 * @file native_buffer.h
 *
 * @brief 定义获取和使用NativeBuffer的相关函数
 *
 * @since 9
 * @version 1.0
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct OH_NativeBuffer;
typedef struct OH_NativeBuffer OH_NativeBuffer;

/**
 * @brief OH_NativeBuffer的属性配置，用于申请新的OH_NativeBuffer实例或查询现有实例的相关属性
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @since 9
 * @version 1.0
 */
typedef struct {
    int32_t width;           // 宽度（像素）
    int32_t height;          // 高度（像素）
    int32_t format;          // 像素格式
    int32_t usage;           // buffer的用途说明
} OH_NativeBuffer_Config;

/**
 * @brief 通过OH_NativeBuffer_Config创建OH_NativeBuffer实例，每次调用都会产生一个新的OH_NativeBuffer实例
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @param config 参数是一个指向OH_NativeBuffer属性的指针，类型为OH_NativeBuffer_Config
 * @return 创建成功则返回一个指向OH_NativeBuffer结构体实例的指针，否则返回NULL
 * @since 9
 * @version 1.0
 */
OH_NativeBuffer* OH_NativeBuffer_Alloc(const OH_NativeBuffer_Config* config);

/**
 * @brief 将OH_NativeBuffer对象的引用计数加1
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @param buffer 参数是一个指向OH_NativeBuffer实例的指针
 * @return 返回一个由GSError定义的int32_t类型的错误码
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Reference(OH_NativeBuffer *buffer);

/**
 * @brief 将OH_NativeBuffer对象的引用计数减1，当引用计数为0的时候，该NativeBuffer对象会被析构掉
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @param buffer 参数是一个指向OH_NativeBuffer实例的指针
 * @return 返回一个由GSError定义的int32_t类型的错误码
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Unreference(OH_NativeBuffer *buffer);

/**
 * @brief 用于获取OH_NativeBuffer的属性
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @param buffer 参数是一个指向OH_NativeBuffer实例的指针
 * @param config 参数是一个指向OH_NativeBuffer_Config的指针，用于接收OH_NativeBuffer的属性
 * @since 9
 * @version 1.0
 */
void OH_NativeBuffer_GetConfig(OH_NativeBuffer *buffer, OH_NativeBuffer_Config* config);

/**
 * @brief 将OH_NativeBuffer对应的ION内存映射到进程空间
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @param buffer 参数是一个指向OH_NativeBuffer实例的指针
 * @param virAddr 参数是一个二级指针，二级指针指向虚拟内存的地址
 * @return 返回一个由GSError定义的int32_t类型的错误码
 * @since 9
 * @version 1.0
 */

int32_t OH_NativeBuffer_Map(OH_NativeBuffer *buffer, void **virAddr);

/**
 * @brief 将OH_NativeBuffer对应的ION内存从进程空间移除
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @param buffer 参数是一个指向OH_NativeBuffer实例的指针
 * @return 返回一个由GSError定义的int32_t类型的错误码
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Unmap(OH_NativeBuffer *buffer);

/**
 * @brief 获取OH_NativeBuffer的序列号
 *
 * @syscap SystemCapability.Graphic.Graphic2D.OH_NativeBuffer
 * @param buffer 参数是一个指向OH_NativeBuffer实例的指针
 * @return 返回对应OH_NativeBuffer的唯一序列号
 * @since 9
 * @version 1.0
 */
uint32_t OH_NativeBuffer_GetSeqNum(OH_NativeBuffer *buffer);

#ifdef __cplusplus
}
#endif

/** @} */
#endif