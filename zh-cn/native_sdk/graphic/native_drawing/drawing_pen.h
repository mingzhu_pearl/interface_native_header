/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PEN_H
#define C_INCLUDE_DRAWING_PEN_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数
 * 
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_pen.h
 *
 * @brief 文件中定义了与画笔相关的功能函数
 *
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 函数用于创建一个画笔对象
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的画笔对象
 * @since 8
 * @version 1.0
 */
OH_Drawing_Pen* OH_Drawing_PenCreate(void);

/**
 * @brief 函数用于销毁画笔对象并回收该对象占有的内存
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenDestroy(OH_Drawing_Pen*);

/**
 * @brief 函数用于获取画笔是否设置抗锯齿属性，如果为真则说明画笔会启用抗锯齿功能，在绘制图形时会对图形的边缘像素进行半透明的模糊处理
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @return 函数返回画笔对象是否设置抗锯齿属性，返回真则设置了抗锯齿，返回假则没有设置抗锯齿
 * @since 8
 * @version 1.0
 */
bool OH_Drawing_PenIsAntiAlias(const OH_Drawing_Pen*);

/**
 * @brief 函数用于设置画笔的抗锯齿属性，设置为真则画笔在绘制图形时会对图形的边缘像素进行半透明的模糊处理
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @param bool 参数真为抗锯齿，参数假则不做抗锯齿处理
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetAntiAlias(OH_Drawing_Pen*, bool);

/**
 * @brief 函数用于获取画笔的颜色属性，颜色属性描述了画笔绘制图形轮廓时使用的颜色，用一个32位（ARGB）的变量表示
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @return 函数返回一个描述颜色的32位（ARGB）变量
 * @since 8
 * @version 1.0
 */
uint32_t OH_Drawing_PenGetColor(const OH_Drawing_Pen*);

/**
 * @brief 函数用于设置画笔的颜色属性，颜色属性描述了画笔绘制图形轮廓时使用的颜色，用一个32位（ARGB）的变量表示
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @param color 参数是一个描述颜色的32位（ARGB）变量
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetColor(OH_Drawing_Pen*, uint32_t color);

/**
 * @brief 函数用于获取画笔的厚度属性，厚度属性描述了画笔绘制图形轮廓的宽度
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @return 函数返回画笔的厚度
 * @since 8
 * @version 1.0
 */
float OH_Drawing_PenGetWidth(const OH_Drawing_Pen*);

/**
 * @brief 函数用于设置画笔的厚度属性，厚度属性描述了画笔绘制图形轮廓的宽度
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @param width 参数是一个描述画笔厚度的变量
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetWidth(OH_Drawing_Pen*, float width);

/**
 * @brief 函数用于获取折线尖角的限制值，当画笔绘制一条折线，转角类型设置为尖角时，那么此时该属性用于限制出现尖角的长度范围，如果超出则平角显示，不超出依然为尖角
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @return 函数返回尖角的限制值
 * @since 8
 * @version 1.0
 */
float OH_Drawing_PenGetMiterLimit(const OH_Drawing_Pen*);

/**
 * @brief 函数用于设置折线尖角的限制值，当画笔绘制一条折线，转角类型设置为尖角时，那么此时该属性用于限制出现尖角的长度范围，如果超出则平角显示，不超出依然为尖角
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @param miter 参数是一个描述尖角限制值的变量
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetMiterLimit(OH_Drawing_Pen*, float miter);

/**
 * @brief 枚举集合定义了画笔笔帽的样式，即画笔在绘制线段时，在线段头尾端点的样式
 * 
 * @since 8
 * @version 1.0
 */
typedef enum {
    /** 没有笔帽样式，线条头尾端点处横切 */
    LINE_FLAT_CAP,
    /** 笔帽的样式为方框，线条的头尾端点处多出一个方框，方框宽度和线段一样宽，高度时线段厚度的一半 */
    LINE_SQUARE_CAP,
    /** 笔帽的样式为圆弧，线条的头尾端点处多出一个半圆弧，半圆的直径与线段厚度一致 */
    LINE_ROUND_CAP
} OH_Drawing_PenLineCapStyle;

/**
 * @brief 函数用于获取画笔笔帽的样式
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @return 函数返回画笔笔帽样式
 * @since 8
 * @version 1.0
 */
OH_Drawing_PenLineCapStyle OH_Drawing_PenGetCap(const OH_Drawing_Pen*);

/**
 * @brief 函数用于设置画笔笔帽样式
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @param OH_Drawing_PenLineCapStyle 参数是一个描述画笔笔帽样式的变量
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetCap(OH_Drawing_Pen*, OH_Drawing_PenLineCapStyle);

/**
 * @brief 枚举集合定义了线条转角的样式，即画笔在绘制折线段时，在折线转角处的样式
 * 
 * @since 8
 * @version 1.0
 */
typedef enum {
    /** 转角类型为尖角，如果折线角度比较小，则尖角会很长，需要使用限制值（miter limit）进行限制 */
    LINE_MITER_JOIN,
    /** 转角类型为圆头 */
    LINE_ROUND_JOIN,
    /** 转角类型为平头 */
    LINE_BEVEL_JOIN
} OH_Drawing_PenLineJoinStyle;

/**
 * @brief 函数用于获取画笔绘制折线转角的样式
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @return 函数返回折线转角的样式
 * @since 8
 * @version 1.0
 */
OH_Drawing_PenLineJoinStyle OH_Drawing_PenGetJoin(const OH_Drawing_Pen*);

/**
 * @brief 函数用于设置画笔绘制转角的样式
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Pen 参数是一个指向画笔对象的指针
 * @param OH_Drawing_PenLineJoinStyle 参数值一个描述折线转角样式的变量
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PenSetJoin(OH_Drawing_Pen*, OH_Drawing_PenLineJoinStyle);

#ifdef __cplusplus
}
#endif
/** @} */
#endif