/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 服务获取驱动对象或者代理后，马达服务启动或停止振动。
 * 通过驱动程序对象或代理提供使用功能。
 *
 * @version 1.0
 */

/**
 * @file vibrator_type.h
 *
 * @brief 定义马达数据结构，包括马达模式和效果振动。
 *
 * @since 2.2
 * @version 1.0
 */

#ifndef VIBRATOR_TYPE_H
#define VIBRATOR_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 定义马达模块返回值。
 *
 * @since 2.2
 */

enum VibratorStatus {
    /** 操作成功。 */
    VIBRATOR_SUCCESS            = 0,
    /** 不支持振动周期设置。 */
    VIBRATOR_NOT_PERIOD         = -1,
    /** 不支持振幅设置。 */
    VIBRATOR_NOT_INTENSITY      = -2,
    /** 不支持频率设置。 */
    VIBRATOR_NOT_FREQUENCY      = -3,
};

/**
 * @brief 定义马达振动模式。
 *
 * @since 2.2
 */

enum VibratorMode {
    /** 表示给定持续时间内的一次性振动。 */
    VIBRATOR_MODE_ONCE   = 0,
    /** 表示具有预置效果的周期性振动。 */
    VIBRATOR_MODE_PRESET = 1,
    /** 表示效果模式无效。 */
    VIBRATOR_MODE_BUTT
};

/**
 * @brief 定义马达参数。
 *
 * 参数包括设置马达振幅和频率以及振幅和频率的范围。
 *
 * @since 3.2
 */
struct VibratorInfo {
    /** 设置马达振幅。1表示支持，0表示不支持。 */
    int32_t isSupportIntensity;
    /** 设置马达频率。1表示支持，0表示不支持。 */
    int32_t isSupportFrequency;
    /** 最大振幅。 */
    int32_t intensityMaxValue;
    /** 最小振幅。 */
    int32_t intensityMinValue;
    /** 最大频率。 */
    int32_t frequencyMaxValue;
    /** 最小频率。 */
    int32_t frequencyMinValue;
};

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* VIBRATOR_TYPE_H */
/** @} */
