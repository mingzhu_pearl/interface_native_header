/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 1.0
 */

/**
 * @file icamera_host_callback.h
 *
 * @brief ICameraHost的回调接口，提供Camera设备和闪关灯状态变化的回调函数，回调函数由调用者实现。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef HDI_CAMERA_HOST_CALLBACK_SERVER_H
#define HDI_CAMERA_HOST_CALLBACK_SERVER_H

#include <list>
#include <map>
#include <vector>
#include "types.h"
#include "icamera_interface.h"

namespace OHOS::Camera {
class ICameraHostCallback : public ICameraInterface {
public:
    /**
     * @brief IPC通信Token校验。
     * @since 1.0
     * @version 1.0
     */
    DECLARE_INTERFACE_DESCRIPTOR(u"HDI.Camera.V1_0.HostCallback");
    virtual ~ICameraHostCallback() {}

public:
    /**
     * @brief 用于Camera设备状态变化时上报状态信息给调用者。
     *
     * @param cameraId [IN] 状态发生变化的Camera设备ID。
     * @param status [IN] 最新的设备状态。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnCameraStatus(const std::string &cameraId, CameraStatus status) = 0;

    /**
     * @brief 用于在闪光灯状态变化时上报状态信息给调用者。
     *
     * @param cameraId [IN] 状态发生变化的闪关灯所绑定的Camera设备ID。
     * @param status [IN] 最新的闪光灯状态。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnFlashlightStatus(const std::string &cameraId, FlashlightStatus status) = 0;
    /**
     * @brief 在相机事件发生时调用。
     *
     * @param cameraId 表示相机事件绑定的相机ID。
     * @param event 表示相机事件类型。
     *
     * @since 1.0
     * @version 1.0
     */
    virtual void OnCameraEvent(const std::string &cameraId, CameraEvent event) = 0;
};
}
#endif /** HDI_CAMERA_HOST_CALLBACK_SERVER_H