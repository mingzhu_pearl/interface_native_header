/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Input
 * @{
 *
 * @brief Input模块驱动接口声明。
 *
 * 本模块为Input服务提供相关驱动接口，包括Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等接口。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file input_reporter.h
 *
 * @brief 描述Input设备数据上报相关的接口声明。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef INPUT_REPORTER_H
#define INPUT_REPORTER_H

#include "input_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供Input设备数据上报相关的接口。
 *
 * 此类接口包含Input设备的数据上报回调函数的注册和注销。
 */
typedef struct {
    /**
     * @brief 注册对应设备的回调函数。
     *
     * Input服务通过此接口注册数据回调函数到hdi中，hdi通过此回调函数上报Input事件。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param callback 输入参数，回调函数的函数指针。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*RegisterReportCallback)(uint32_t devIndex, InputEventCb *callback);

    /**
     * @brief 注销对应设备的回调函数。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*UnregisterReportCallback)(uint32_t devIndex);

    /**
     * @brief 注册Input设备的热插拔回调函数。
     *
     * Input服务通过此接口注册回调函数到hdi中，所有Input设备由此函数进行热插拔事件上报。
     *
     * @param callback 输入参数，回调函数的函数指针。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*RegisterHotPlugCallback)(InputHostCb *callback);

    /**
     * @brief 注销Input设备的热插拔回调函数。
     *
     * @param 无。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetSatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*UnregisterHotPlugCallback)(void);
} InputReporter;

#ifdef __cplusplus
}
#endif
#endif
/** @} */