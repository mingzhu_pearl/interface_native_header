/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN模块向上层WLAN服务提供了统一接口。
 *
 * HDI层开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描，关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file wifi_hal_ap_feature.h
 *
 * @brief 提供WLAN的AP特性能力（获取与AP连接的STA的基本信息、设置国家码）。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef WIFI_HAL_AP_FEATURE_H
#define WIFI_HAL_AP_FEATURE_H

#include "wifi_hal_base_feature.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

/**
 * @brief 描述与AP连接的STA的基本信息
 *
 * @since 1.0
 * @version 1.0
 */
struct StaInfo {
    /**< STA的MAC地址 */
    unsigned char mac[WIFI_MAC_ADDR_LENGTH];
};

/**
 * @brief 继承了{@link IWiFiBaseFeature}基本特性，并包含AP模式下获取连接STA的信息和设置国家码的功能。
 *
 * @since 1.0
 * @version 1.0
 */
struct IWiFiAp {
    /**< 基本特性{@link IWiFiBaseFeature} */
    struct IWiFiBaseFeature baseFeature;

    /**
     * @brief 获取连接上的所有STA的信息（目前只包含MAC地址）。
     *
     * @param apFeature 输入参数，AP特性{@link IWiFiAp}。
     * @param staInfo 输出参数，保存与AP连接的STA的基本信息。
     * @param count 输入参数，staInfo结构体数组的元素个数。
     * @param num 输出参数，实际连接的STA的个数。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getAsscociatedStas)(const struct IWiFiAp *apFeature, struct StaInfo *staInfo,
        uint32_t count, uint32_t *num);

    /**
     * @brief 设置国家码（表示AP射频所在的国家，规定了AP射频特性，包括AP的发送功率、支持的信道等。其目的是为了使AP的射频特性符合不同国家或区域的法律法规要求）。
     *
     * @param apFeature 输入参数，AP特性{@link IWiFiAp}。
     * @param code 输入参数，设置的国家码。
     * @param len 输入参数，国家码长度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*setCountryCode)(const struct IWiFiAp *apFeature, const char *code, uint32_t len);
};

/**
 * @brief 初始化AP特性。WLAN服务在创建AP类型的特性{@link FeatureType}时调用。
 *
 * @param fe 输入参数，AP特性{@link IWiFiAp}。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t InitApFeature(struct IWiFiAp **fe);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif
/** @} */
